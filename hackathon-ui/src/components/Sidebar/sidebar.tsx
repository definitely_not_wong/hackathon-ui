import React, { useEffect, useContext, useState } from "react";
import { Divider, List, ListItem, Typography, ListItemText, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';

import {TradeType} from "../../types";
import {ApiContext} from "../../utils/apiContext";


interface SidebarProps {
  selectStock: (trade: TradeType | null) => void;
  portfolio: TradeType[];
}

const useStyles = makeStyles({
  listItem: {
    display: 'flex',
    justifyContent: 'space-between'
  }
});



function Sidebar({ selectStock, portfolio }: SidebarProps) {
  const [trades, setTrades] = useState<TradeType[]>(portfolio);
  const [selectedTrade, setSelectedTrade] = useState<TradeType | null>(null);

  const apiContext = useContext(ApiContext);
  const classes = useStyles();

  const clickStock = (stock: TradeType | null) => () => {
    selectStock(stock);
    setSelectedTrade(stock);
  };

  useEffect(() => {
    setTrades(portfolio);
  }, [portfolio]);

  return (
    <List component={Paper} >
      {/*//style={{backgroundColor: "#f5f5f5"}}*/}
      <ListItem
        className={classes.listItem}
        button
        onClick={clickStock(null)}
        selected={selectedTrade === null}
      >
        <ListItemText primary="Summary" />
        <HomeIcon />
      </ListItem>
      <ListItem
        className={classes.listItem}
      >
        <Typography>
          Stock
        </Typography>
        <Typography>
          Holdings
        </Typography>
      </ListItem>
      {trades.map((trade, index) => {
        return (
          <>
            <ListItem
              key={trade.ticker}
              button
              onClick={clickStock(trade)}
              className={classes.listItem}
              selected={selectedTrade !== null && selectedTrade.ticker === trade.ticker}
            >
              <Typography>
                {trade.ticker}
              </Typography>
              <Typography>
                {trade.quantity}
              </Typography>
            </ListItem>
            { index < trades.length - 1 && <Divider /> }
          </>
        )
      })}
    </List>
  );
}

export default Sidebar;






