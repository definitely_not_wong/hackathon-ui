import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TradeAction } from '../../types';
import { NewTradeForm } from './maketrade';
import formatMoney from '../../utils/formatMoney';

interface ConfirmationDialogProps {
  tradeInfo: NewTradeForm;
  open: boolean;
  confirm: (confirmTrade: boolean) => void;
}

export default function ConfirmationDialog({ open, confirm, tradeInfo }: ConfirmationDialogProps) {
  const {tradeAction, volume, tickerName, price} = tradeInfo;

  return (
    <div>
      <Dialog
        open={open}
      >
        <DialogTitle>Confirm trade:</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {`Are you sure you want ${tradeAction.toLowerCase()} ${volume} stocks of ${tickerName} for ${formatMoney(Number(price))}.?`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => confirm(false)} color="primary" autoFocus>
            Cancel
          </Button>
          <Button onClick={() => confirm(true)} color="primary">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
