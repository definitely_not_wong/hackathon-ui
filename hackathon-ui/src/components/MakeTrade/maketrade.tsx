import React, { useContext, useEffect, useState } from "react";
import { FormControlLabel, FormHelperText, FormLabel, Radio, RadioGroup, TextField } from "@material-ui/core";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';

import './makeTrade.css';
import { NewTradeInfo, TradeAction, TradeType } from "../../types";
import ConfirmationDialog from '../MakeTrade/confirmationDialog';
import CurrencyInput from '../../utils/currencyInput';
import VolumeInput from '../../utils/volumeInput';
import { ApiContext, LIVE_PRICE_EVENT } from '../../utils/apiContext';
import TickerAutoFill from '../../utils/tickerAutoFill';
import formatMoney from "../../utils/formatMoney";

interface MakeTradeProps {
  initialTickerName?: string;
}

export interface NewTradeForm extends Omit<NewTradeInfo, 'price' | 'volume'> {
  price: string;
  volume: string;
}

type TransactionType = 'Volume' | 'Price';

const INITIAL_TRADE_INFO: NewTradeForm = {
  tickerName: '',
  tradeAction: 'Buy',
  volume: '',
  price: ''
};

const INITIAL_FORM_ERROR = {
  tickerName: false,
  tradeAction: false,
  volume: false,
  price: false
};

const MakeTrade = ({ initialTickerName = '' }: MakeTradeProps) => {
  const [tradeInfo, setTradeInfo] = useState<NewTradeForm>({...INITIAL_TRADE_INFO, tickerName: initialTickerName});
  const [errorInfo, setErrorInfo] = useState(INITIAL_FORM_ERROR);
  const [openDialog, setOpenDialog] = useState(false);
  const [transactionType, setTransactionType] = useState<TransactionType>('Volume');
  const [lastPrice, setLastPrice] = useState(0);

  const apiContext = useContext(ApiContext);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    if (event.target !== null) {
      setTradeInfo(prevState => ({
        ...prevState,
        [event.target.name]: event.target.value
      }))
    }
  };

  const setTickerName = (tickerName: string) => {
    setTradeInfo(prevState => ({
      ...prevState,
      tickerName
    }))
  }

  const handleTransactionType = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTransactionType(event.target.value as TransactionType);
  };

  const errorExists = () => {
    let error = false;
    setErrorInfo((prevState) => {
      const newErrorState = {...prevState};

      Object.keys(tradeInfo).forEach((tradeKey) => {
        if (tradeKey === 'volume' && transactionType === 'Volume' && tradeInfo.volume === '') {
          newErrorState.volume = true;
          error = true;
        } else if (tradeKey === 'price' && transactionType === 'Price' && tradeInfo.price === '') {
          newErrorState.price = true;
          error = true;
        } else if (tradeKey === 'tickerName' && tradeInfo.tickerName === '') {
          newErrorState.tickerName = true;
          error = true;
        } else {
          // @ts-ignore
          newErrorState[tradeKey] = false;
        }
      });

      console.log(newErrorState);
      console.log(tradeInfo);

      return newErrorState;
    });

    return error;
  }

  const handleSubmit = (tradeAction: TradeAction) => () => {
    if (errorExists()) {
      return;
    }

    setTradeInfo(prevState => {
      let price = prevState.price;
      let volume = prevState.volume;

      if (transactionType === 'Volume') {
        price = `${lastPrice * Number(volume)}`;
      } else {
        volume = `${Number(price) / lastPrice}`;
      }
      return {
        ...prevState,
        tradeAction,
        price,
        volume
      }
    });

    setOpenDialog(true);
  };

  const confirmTrade = (confirmTrade: boolean) => {
    if (confirmTrade) {
      apiContext.createTrade(tradeInfo.tickerName, Number(tradeInfo.volume), Number(tradeInfo.price), tradeInfo.tradeAction);
      setTradeInfo(prevState => ({...INITIAL_TRADE_INFO, tickerName: prevState.tickerName }))
    }

    setOpenDialog(false);
  };

  useEffect(() => {
    const { tickerName } = tradeInfo;
    if (tickerName !== '') {
      apiContext
        .getStockInfo(tickerName)
        .then((data) => {
          if (data) {
            setLastPrice(data.bid);
          }
        })
    }
  }, [tradeInfo, apiContext]);

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          Make Trade
        </Typography>
        <form className="make-trade-form">
          <FormControl error={errorInfo.tickerName}>
            <TickerAutoFill selectTickerName={setTickerName} />
            {errorInfo.tickerName && <FormHelperText>Please enter a ticker name</FormHelperText>}
          </FormControl>
          <FormControl
            component="fieldset"
          >
            <FormLabel component="legend">Trade type:</FormLabel>
            <RadioGroup
              value={transactionType}
              onChange={handleTransactionType}
              row
            >
              <FormControlLabel
                value="Volume"
                control={<Radio />}
                label="Volume"
              />
              <FormControlLabel
                value="Price"
                control={<Radio />}
                label="Price"
              />
            </RadioGroup>
          </FormControl>
          <div style={{ display: 'flex', alignItems: 'baseline', justifyContent: 'flex-start'}}>
            {
              transactionType === 'Volume'
                ? (
                  <>
                    <TextField
                      error={errorInfo.volume}
                      name="volume"
                      value={tradeInfo.volume === '' ? null : tradeInfo.volume}
                      onChange={handleChange}
                      label="Shares"
                      style = {{width: '50%'}}
                      InputProps={{
                        inputComponent: VolumeInput as any
                      }}
                      helperText={errorInfo.volume ? 'Please enter number of shares.' : ''}
                    />
                    <p style={{float: 'right', marginRight : 10, marginTop: 20, marginLeft: -30}}>x ${lastPrice} = {formatMoney(lastPrice * Number(tradeInfo.volume === '' ? '0' : tradeInfo.volume))}</p>
                  </>
                )
                : (
                  <>
                    <TextField
                      error={errorInfo.price}
                      label="Price"
                      value={tradeInfo.price === '' ? null : tradeInfo.price}
                      name="price"
                      style = {{width: '40%'}}
                      InputProps={{
                        inputComponent: CurrencyInput as any
                      }}
                      onChange={handleChange}
                      helperText={errorInfo.price ? 'Please enter the amount you want to pay.' : ''}
                    />
                    <p style={{float: 'right', marginRight : 10, marginTop: 20, marginLeft: -10}}>/ ${lastPrice} = {((Number(tradeInfo.price === '' ? '0' : tradeInfo.price) / lastPrice) || 0).toFixed(5) } shares</p>
                  </>
                )
            }
          </div>

          <br/>
          <div className="make-trade-submit">
            <Button
              variant="contained"
              style={{ backgroundColor: '#008c00', color: 'white', marginRight: 8 }}
              onClick={handleSubmit('Buy')}
            >
              Buy
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit('Sell')}
            >
              Sell
            </Button>
          </div>
        </form>
        <br/>
        <ConfirmationDialog
          tradeInfo={tradeInfo}
          open={openDialog}
          confirm={confirmTrade}
        />
      </CardContent>
    </Card>
  )
};

export default MakeTrade;