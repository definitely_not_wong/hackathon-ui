import React, { useContext, useEffect, useState } from 'react';
import './App.css';
import {
  Row,
  Col,
} from 'reactstrap';
import { Paper, Typography } from '@material-ui/core';

import TickerInfo from "../TickerInfo/tickerInfo";
import TickerHeader from "../TickerInfo/tickerheader";
import {ApiContext} from "../../utils/apiContext";
import Sidebar from '../Sidebar/sidebar';
import MakeTrade from '../MakeTrade/maketrade';
import 'bootstrap/dist/css/bootstrap.min.css';
import {TradeHistoryType, TradeType} from '../../types';
import BollingerGraph from '../BollingerGraph/bollingerGraph';
import stonks from '../../img/stonks.jpg'
import LiveTicker from '../LiveTicker/liveTicker';
import DonutGraph from '../Home/donutGraph';
import Watchlist from '../LiveTicker/watchlist';
import TradeHistory from '../TickerInfo/tradeHistory';
import AppBar from '../Home/appBar';
import MajorIndicies from './majorIndicies';

function App() {
  const [currentTrade, setCurrentTrade] = useState<TradeType | null>(null);
  const [portfolio, setPortfolio] = useState<TradeType[]>([]);
  const [allTradeHistory, setAllTradeHistory] = useState<TradeHistoryType[]>([]);
  const apiContext = useContext(ApiContext);

  useEffect(() => {
    apiContext
        .getPortfolio()
        .then((tradeData) => {
          console.log(tradeData);
          setPortfolio(tradeData);
        })
  }, [apiContext]);

  useEffect(() => {
    apiContext
        .getPortfolio()
        .then((tradeData) => {
          console.log(tradeData);
          setPortfolio(tradeData);
        })
  }, [apiContext]);

  useEffect(() => {
    const newTradeHistory: TradeHistoryType[] = [];
    portfolio.forEach((stock) => {
      stock.tradeHistory.forEach((tradeHistory) => {
        newTradeHistory.push(tradeHistory);
      });
      stock.pending.forEach((pendingTrade) => {
        newTradeHistory.push(pendingTrade);
      })
    })
    setAllTradeHistory(newTradeHistory);
  }, [portfolio]);

  return (
      <div className="App">
        <Row>
          <AppBar />
        </Row>
        <div className="body">
          <Row>
            <Col md={3} class="col-md-3">
              <br />
              <Sidebar
                  selectStock={setCurrentTrade}
                  portfolio={portfolio}
              />
              <br />
              <Watchlist portfolio={portfolio}/>
            </Col>
            <Col md={6} class="col-md-6">
              <br />
              { currentTrade
                  ? (
                      <div>
                        <TickerHeader trade={currentTrade} />
                        <br/>
                        <TickerInfo trade={currentTrade} />
                      </div>
                  )
                  : (
                      <>
                        <DonutGraph portfolio={portfolio}/>
                        <br />
                        <TradeHistory tradeHistory={allTradeHistory} name={true} />
                      </>
                  )
              }
            </Col>
            <Col md={3} class="col-md-3">
              <br />
              <MakeTrade/>
              <br />
              <MajorIndicies />
              <br/>
              { currentTrade && <BollingerGraph trade={currentTrade}/> }
            </Col>
          </Row>
        </div>
      </div>
  );
}

export default App;
