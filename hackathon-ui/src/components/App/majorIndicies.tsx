import React from 'react';
import { Paper, Typography } from '@material-ui/core';
import LiveTicker from '../LiveTicker/liveTicker';

const MajorIndicies = () => {
  return (
    <Paper >
      <Typography variant="h5" component="h2" style={{ padding: 16}}>
        Major Indicies
      </Typography>
      <div style={{ display: 'flex', justifyContent: 'space-evenly', flexWrap: 'wrap'}}>
        <LiveTicker tickerName="^GSPC" />
        <LiveTicker tickerName="^IXIC" />
        <LiveTicker tickerName="^RUT" />
      </div>
    </Paper>
  )
}

export default MajorIndicies;