import React, {ReactNode, useEffect, useState} from 'react';
import {Paper, Table, TableContainer, TableCell, TableRow, TableBody} from "@material-ui/core";
import { format, parseISO } from 'date-fns';

import {TradeHistoryType, TradeType} from "../../types";
import SortableHeader from "../../utils/sortableHeader";
import sortRows, {getComparator} from "../../utils/sortRows";
import formatMoney from "../../utils/formatMoney";

export type Order = 'asc' | 'desc';
export type TradeTypeKeys = keyof TradeHistoryType;
export interface HeadCell {
    disablePadding: boolean;
    id: TradeTypeKeys;
    label: string;
    align: 'inherit' | 'left' | 'center' | 'right' | 'justify';
}


interface TradeHistoryProps {
    tradeHistory: TradeHistoryType[];
    name?: boolean;
}


const initialHeadCell: HeadCell[] = [
    { id: 'date', align: 'left', disablePadding: false, label: 'Trade Date' },
    { id: 'stockQuantity', align: 'right', disablePadding: false, label: 'Quantity' },
    { id: 'requestedPrice', align: 'right', disablePadding: false, label: 'Price' },
    { id: 'status', align: 'right', disablePadding: false, label: 'Status' }
];


const formatDate = (date: string) => {
    return format(parseISO(date), 'MM-dd-yyyy');
}

const TradeHistory = ({ tradeHistory, name = false }: TradeHistoryProps) => {
    const [orderBy, setOrderBy] = useState<TradeTypeKeys>('date');
    const [order, setOrder] = React.useState<Order>('desc');
    const [historyRows, setHistoryRows] = useState<ReactNode[]>([]);
    const [headCells, setHeadCells] = useState<HeadCell[]>(initialHeadCell);

    const handleRequestSort = (event: React.MouseEvent<unknown>, property: TradeTypeKeys) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    useEffect(() => {
        if (name) {
            setHeadCells([
                { id: 'date', align: 'left', disablePadding: false, label: 'Trade Date' },
                { id: 'ticker', align: 'right', disablePadding: false, label: 'Name' },
                { id: 'stockQuantity', align: 'right', disablePadding: false, label: 'Quantity' },
                { id: 'requestedPrice', align: 'right', disablePadding: false, label: 'Price' },
                { id: 'status', align: 'right', disablePadding: false, label: 'Status' }
            ])
        } else {

        }
    }, [name]);


    console.log(historyRows.length, historyRows);

    return (
        <TableContainer component={Paper}>
            <Table >
                <SortableHeader
                    headCells={headCells}
                    onRequestSort={handleRequestSort}
                    order={order}
                    orderBy={orderBy}
                />
                <TableBody>
                    {sortRows(tradeHistory, getComparator(order, orderBy))
                        .map((row) => (
                            <TableRow key={row.id.timestamp+row.ticker+row.status}>
                                <TableCell align="left">{formatDate(row.id.date)}</TableCell>
                                { name && <TableCell align="right">{row.ticker}</TableCell> }
                                <TableCell align="right">{row.stockQuantity}</TableCell>
                                <TableCell align="right">{formatMoney(row.requestedPrice)}</TableCell>
                                <TableCell align="right">{row.status}</TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default TradeHistory;
