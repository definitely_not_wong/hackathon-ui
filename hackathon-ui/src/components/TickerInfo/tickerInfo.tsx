import React, {useEffect} from "react";

import {TradeType, TradeHistoryType} from "../../types";
import {useState} from "react";
import TradeHistory from "./tradeHistory";
import TickerChart from './tickerChart';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

interface TickerInfoProps {
    trade: TradeType;
}

const TickerInfo = ({ trade }: TickerInfoProps) => {
    const [tradeHistory, setTradeHistory] = useState<TradeHistoryType[]>([]);

    useEffect(() => {
        console.log(trade);
        const newTradeHistory: TradeHistoryType[] = [];
        trade.tradeHistory.forEach((tradeHistory) => {
            newTradeHistory.push({...tradeHistory});
        });
        trade.pending.forEach((pendingTrade) => {
            newTradeHistory.push({...pendingTrade});
        })
        setTradeHistory(newTradeHistory);
    }, [trade]);

    return (
        <div>
            <Card>
                <CardContent>
                    <TickerChart tickerName={trade.ticker} />
                </CardContent>
                <TradeHistory tradeHistory={tradeHistory} />
            </Card>
        </div>
    )
}

export default TickerInfo;
