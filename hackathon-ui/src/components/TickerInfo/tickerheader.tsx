import React, { useContext, useEffect } from "react";

import { StockInfoType, TradeType } from "../../types";
import {useState} from "react";
import TradeHistory from "./tradeHistory";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { ApiContext, LIVE_PRICE_EVENT } from "../../utils/apiContext";

import './tickerheader.css';
import formatMoney from '../../utils/formatMoney';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

interface TickerInfoProps {
    trade: TradeType;
}

const useStyles = makeStyles({
    header: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    nameSection: {
        display: 'flex',

        '& > div': {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            marginLeft: 16
        }
    },
    valueSection: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end'
    }
});

const PRECISION = 2;
const POSITIVE_COLOR = '#03883e';
const NEGATIVE_COLOR = '#ed2a41';

const percentChange = (original: number, newValue: number) => {
  if (original === 0) {
    return 0;
  }

  return ((newValue - original) / original * 100).toFixed(PRECISION);
};

const tickerColor = (original: number, newValue: number) => {
  if (newValue > original) {
    return POSITIVE_COLOR;
  }

  return NEGATIVE_COLOR;
};


const TickerHeader = ({ trade }: TickerInfoProps) => {
    const [stockInfo, setStockInfo] = useState<StockInfoType | null>(null);
    const [currentBid, setCurrentBid] = useState(0);

    const apiContext = useContext(ApiContext);

    const classes = useStyles();

    useEffect(() => {
        apiContext
          .getStockInfo(trade.ticker)
          .then((data) => {
              setStockInfo(data);
          })
    }, [apiContext, trade]);

    useEffect(() => {
        const socket = apiContext.getLivePriceSocket();
        const event_name = `${trade.ticker} price`;

        socket.emit(LIVE_PRICE_EVENT, trade.ticker);

        socket.on(event_name, (response: any) => {
            setCurrentBid(response.bid);

            return () => {
                console.log('disconnected');
                socket.disconnect();
            }
        })
    }, [apiContext, trade.ticker]);

    return (
      <div>
          <Card>
              <CardContent>
                  { stockInfo !== null && (
                    <div className={classes.header}>
                        <div className={classes.nameSection}>
                            <img style={{ height: 64, width: 64 }} src={stockInfo?.logo_url} />
                            <div >
                                <Typography variant="h6">{trade.ticker}</Typography>
                                <Typography variant="h6">{stockInfo?.longName}</Typography>
                            </div>
                        </div>
                        <div className={classes.valueSection}>
                            <Typography variant="h6">{formatMoney(trade.quantity * currentBid)}</Typography>
                            <Typography
                              variant="h6"
                              style={{ color: tickerColor(stockInfo?.previousClose, currentBid)}}
                            >
                                {`${(currentBid-stockInfo?.previousClose).toFixed(PRECISION)} (${percentChange(stockInfo?.previousClose, currentBid)}%)`}
                            </Typography>
                        </div>
                    </div>
                  )}
              </CardContent>
          </Card>
      </div>
    )
}

export default TickerHeader;
