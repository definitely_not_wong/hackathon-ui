import React, { useContext, useEffect, useMemo, useState } from 'react';
import Chart from "react-apexcharts";

import { ApiContext } from '../../utils/apiContext';
import { ButtonGroup, Typography } from '@material-ui/core';
import Button from 'reactstrap/lib/Button';

import './tickerChart.css';

interface TickerChartProps {
  tickerName: string;
}

const PERIOD_OPTIONS = [
  {
    label: 'Day',
    value: '1d'
  },
  {
    label: 'Week',
    value: '5d'
  },
  {
    label: 'Month',
    value: '1mo'
  },
  {
    label: 'Year',
    value: '1y'
  },
];

const INTERVAL_OPTIONS = [
  {
    label: '1m',
    value: '1m'
  },
  {
    label: '1h',
    value: '1h'
  },
  {
    label: '1d',
    value: '1d'
  }
];

const TickerChart = ({ tickerName }: TickerChartProps) => {
  const [data, setData] = useState<any>([]);
  const [period, setPeriod] = useState(PERIOD_OPTIONS[0].value);
  const [interval, setInterval] = useState(INTERVAL_OPTIONS[0].value);

  const apiContext = useContext(ApiContext);

  const series = useMemo(() => (
      [{
        data: data
      }]
  ), [data]);

  const options = {
    chart: {
      type: 'candlestick',
      height: 350,
    },
    xaxis: {
      type: 'datetime'
    },
    yaxis: {
      tooltip: {
        enabled: true
      }
    }
  };

  const periodGroup = useMemo(() => (
      <div>
        <Typography>Date range</Typography>
        <ButtonGroup>
          {PERIOD_OPTIONS.map((periodOption) => (
              <Button
                  key={periodOption.label}
                  onClick={() => setPeriod(periodOption.value)}
                  color={period === periodOption.value ? 'primary' : 'secondary'}
              >
                {periodOption.label}
              </Button>
          ))}
        </ButtonGroup>
      </div>
  ), [period]);

  const intervalGroup = useMemo(() => (
      <div>
        <Typography>Interval range</Typography>
        <ButtonGroup size="small">
          {INTERVAL_OPTIONS.map((intervalOption) => (
              <Button
                  key={intervalOption.label}
                  onClick={() => setInterval(intervalOption.value)}
                  color={interval === intervalOption.value ? 'primary' : 'secondary'}
              >
                {intervalOption.label}
              </Button>
          ))}
        </ButtonGroup>
      </div>
  ), [interval]);

  useEffect(() => {
    apiContext
        .getLiveTradeHistory({
          tickerName: tickerName,
          interval,
          datePeriod: period
        })
        .then((responseData) => {
          setData(responseData);
        })
  }, [apiContext, interval, period, tickerName]);

  return (
      <div id="chart">
        <Chart options={options} series={series} type="candlestick" height={350} />
        <div className="chart-buttons">
          {periodGroup}
          {intervalGroup}
        </div>
      </div>
  )
};

export default TickerChart;