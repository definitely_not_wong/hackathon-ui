import React, { useContext, useEffect, useMemo, useState } from 'react';
import Chart from "react-apexcharts";
import { sub } from "date-fns";
import { ButtonGroup, Typography, Card, CardContent, CardHeader } from '@material-ui/core';

import { ApiContext } from '../../utils/apiContext';
import { TERM_TYPES, TermType } from '../../utils/termType';
import Button from 'reactstrap/lib/Button';
import formatMoney from '../../utils/formatMoney';
import { TradeType } from '../../types';

interface BollingerGraphProps {
    trade: TradeType;
}


const BollingerGraph = ({ trade }: BollingerGraphProps) => {
    const [series, setSeries] = useState<any[]>([]);
    const [term, setTerm] = useState<TermType>('short');

    const apiContext = useContext(ApiContext);

    const termGroup = useMemo(() => (
        <div>
            <Typography>Term type</Typography>
            <ButtonGroup size="small">
                {TERM_TYPES.map((termOption) => (
                    <Button
                        key={termOption}
                        onClick={() => setTerm(termOption)}
                        color={term === termOption ? 'primary' : 'secondary'}
                    >
                        {termOption}
                    </Button>
                ))}
            </ButtonGroup>
        </div>
    ), [term]);

    useEffect(() => {
        apiContext
            .getBollingerData(trade.ticker, term)
            .then((data) => {
                const { type, avg, upperBand, lowerBand, close } = data;
                setSeries([
                    {
                        name: 'Upper Bound',
                        data: upperBand,
                        type: 'area'
                    },
                    {
                        name: type,
                        data: avg,
                        type: 'line'
                    },
                    {
                        name: 'Close',
                        data: close,
                        type: 'line',
                    },
                    {
                        name: 'Lower Bound',
                        data: lowerBand,
                        type: 'area'
                    },
                ]);
            })
    }, [trade, apiContext, term]);

    return (
        <Card>
            <CardHeader title="Bollinger Graph" />
            <CardContent>
                <Chart
                    options={{
                        chart: {
                            animations: {
                                enabled: true
                            },
                            type: 'line',
                            height: 350,
                            zoom: {
                                enabled: true
                            },
                            events: {
                                beforeZoom: function(ctx: any) {
                                    // we need to clear the range as we only need it on the iniital load.
                                    ctx.w.config.xaxis.range = undefined
                                }
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'straight',
                            width: [0, 3, 3, 0]
                        },
                        colors: ['#808080','#000000', '#FF0000', '#ffffff'],
                        fill: {
                            type: ['solid', 'solid',  'solid', 'solid'],
                            opacity: 1,
                            gradient: {
                                opacityFrom: 0,
                                opacityTo: 0
                            }
                        },
                        xaxis: {
                            type: 'datetime',
                            min: sub(new Date(), { months: 3}).getTime()
                        },
                        yaxis: {
                            opposite: true,
                            labels: {
                                formatter: (value: any) => { return formatMoney(Number(value)) },
                            }
                        },
                        legend: {
                            horizontalAlign: 'left'
                        }
                    }}
                    series={series}
                    type='line'
                    height={350} />
                {termGroup}
            </CardContent>
        </Card>
    );
};

export default BollingerGraph;
