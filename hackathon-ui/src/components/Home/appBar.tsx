import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

export default function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ backgroundColor: '#262626' }}>
        <Toolbar style={{ display: 'flex', justifyContent: 'space-between'}}>
          <div style={{ display: 'flex', alignItems: 'baseline' }}>
            <img alt="Citi Logo" style={{ height: 64, margin: 8 }} src="https://seeklogo.com/images/C/citi-logo-215845CA7C-seeklogo.com.png"/>
            <Typography variant="h4" className={classes.title}>
              PJAM
            </Typography>
          </div>
          <div>
            <Button color="inherit" href="https://i.kym-cdn.com/entries/icons/original/000/029/959/Screen_Shot_2019-06-05_at_1.26.32_PM.jpg">Logout</Button>
            <AccountCircle />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
