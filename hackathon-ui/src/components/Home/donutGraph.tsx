import React, { useContext, useEffect, useMemo, useState } from 'react';
import Chart from "react-apexcharts";
import { ApiContext } from '../../utils/apiContext';
import { BollingerDataResponse, TickerNameResult, TradeType } from "../../types";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

interface DonutChartProps {
    portfolio: TradeType[];
}

var dummy = [
    {id:1, name:'Sandra', type:'user', username:'sandra'},
    {id:2, name:'John', type:'admin', username:'johnny2'},
    {id:3, name:'Peter', type:'user', username:'pete'},
    {id:4, name:'Bobby', type:'user', username:'be_bob'}];


var processedData = [];



const DonutChart = ({ portfolio }: DonutChartProps) => {
    const series = portfolio.map(t => t.quantity);
    const labels = portfolio.map(t => t.ticker);
  
    const options = {
        chart: {
            type: 'donut',
            height: 350,
        },
        labels: labels,
        dataLabels: {
            enabled: true
        },
        responsive: [{
            breakpoint: 480,
            options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
            }
        }]
    };

    return (
        <Card>
            <CardContent>
                <h4>Portfolio Makeup</h4>
                <div id="chart">
                    <Chart options={options} series={series} type="donut" height={350} />
                </div>
            </CardContent>
        </Card>
    )
  };
  
  export default DonutChart;