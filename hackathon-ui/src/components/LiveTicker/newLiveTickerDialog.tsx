import React, { useState } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TickerAutoFill from '../../utils/tickerAutoFill';
import { TradeAction, TradeType } from '../../types';
import { createEmptyTickerNameResult } from '../../utils/createEmptyTickerNameResult';

interface NewLiveTickerDialogProps {
  open: boolean;
  confirm: (newTickerName: string) => void;
  portfolio: TradeType[];
}


const NewLiveTickerDialog = ({ open, confirm, portfolio }: NewLiveTickerDialogProps) => {
  const [tickerName, setTickerName] = useState('');

  const submit = (submitName: boolean) => () => {
    if (submitName) {
      confirm(tickerName);
    } else {
      confirm('');
    }

    setTickerName('');
  };

  const initialOptions = portfolio.map((trade) => createEmptyTickerNameResult(trade.ticker));

  return (
    <Dialog
        open={open}
      >
        <DialogTitle>Choose a stock:</DialogTitle>
        <DialogContent>
          <TickerAutoFill selectTickerName={setTickerName} initialOptions={initialOptions}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={submit(false)} color="primary" autoFocus>
            Cancel
          </Button>
          <Button onClick={submit(true)} color="primary">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
  )
};

export default NewLiveTickerDialog;