import React, { useContext, useEffect, useMemo, useState } from 'react';
import { ApiContext, LIVE_PRICE_EVENT } from '../../utils/apiContext';
import Chart from "react-apexcharts";
import formatMoney from '../../utils/formatMoney';
import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

interface LiveTickerProps {
  tickerName: string;
  padding?: number;
}

type TickerInfo = {
  prevClose: number,
  series: number[][]
}

const useStyles = makeStyles({
  paper: {
    width: 168,
    height: 168,
    display: 'flex',
    flexDirection: 'column',
    margin: '8px 0 8px 21.33px',
  },
  data: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '8px 8px -16px'
  }
});

const EMPTY_TICKER_INFO: TickerInfo= {
  prevClose: 0,
  series: []
};
const MAX_DATA_COUNT = 20;
const PRECISION = 2;
const POSITIVE_COLOR = '#03883e';
const NEGATIVE_COLOR = '#ed2a41';
const PADDING = 27.125;

const percentChange = (original: number, newValue: number) => {
  if (original === 0) {
    return 0;
  }

  return ((newValue - original) / original * 100).toFixed(PRECISION);
};

const tickerColor = (original: number, newValue: number) => {
  if (newValue > original) {
    return POSITIVE_COLOR;
  }

  return NEGATIVE_COLOR;
};

const LiveTicker = ({tickerName, padding = 0}: LiveTickerProps) => {
  const [tickerInfo, setTickerInfo] = useState<TickerInfo>({...EMPTY_TICKER_INFO});
  const apiContext = useContext(ApiContext);

  const classes = useStyles();

  const currentBid = useMemo(() => {
    const { series } = tickerInfo;
    if (series.length === 0) {
      return 0;
    }

    return series[tickerInfo.series.length - 1][1];
  }, [tickerInfo]);

  const options = {
    chart: {
      type: 'area',
      stacked: false,
      height: 128,
      width: 128,
      zoom: {
        enabled: false,
      },
      toolbar: {
        enabled: false,
        tools: {
          download: false
        }
      }
    },
    dataLabels: {
      enabled: false
    },
    fill: {
      gradient: {
        enabled: true,
        opacityFrom: 0.55,
        opacityTo: 0
      }
    },
    colors: [tickerColor(tickerInfo.prevClose, currentBid)],
    markers: {
      enabled: false
    },
    stroke: {
      show: true,
      curve: 'straight',
      color: tickerColor(tickerInfo.prevClose, currentBid)
    },
    tooltip: {
      enabled: false
    },
    yaxis: {
      labels: {
        show: false
      }
    },
    xaxis: {
      labels: {
        show: false
      }
    }
  };

  useEffect(() => {
    const socket = apiContext.getLivePriceSocket();
    const event_name = `${tickerName} price`;

    socket.emit(LIVE_PRICE_EVENT, tickerName);

    socket.on(event_name, (response: any) => {
      setTickerInfo(prevState => {
        const { series } = prevState;
        const newSeries = [...series, [new Date().getTime(), response.bid]];

        if (newSeries.length > MAX_DATA_COUNT) {
          newSeries.shift()
        }

        return {
          prevClose: response.prevClose,
          series: newSeries
        }
      });
      console.log(response);
    });

    return () => {
      console.log('disconnected');
      socket.disconnect();
    }
  }, [apiContext, tickerName]);

  return (
    <Paper
      elevation={0}
      variant="outlined"
      className={classes.paper}
      style={{
        marginLeft: padding,
        border: '4px solid rgba(0, 0, 0, 0.12)'
      }}
    >
      <div className={classes.data}>
        <div>
          <Typography align="left">
            {tickerName}
          </Typography>
          <Typography variant="body2" align="left">
            {formatMoney(tickerInfo.prevClose)}
          </Typography>
        </div>
        <div>
          <Typography align="right" style={{ color: tickerColor(tickerInfo.prevClose, currentBid) }}>
            {(currentBid - tickerInfo.prevClose).toFixed(PRECISION)}
          </Typography>
          <Typography align="right" style={{ color: tickerColor(tickerInfo.prevClose, currentBid) }}>
            {`${percentChange(tickerInfo.prevClose, currentBid)}%`}
          </Typography>
        </div>
      </div>
      <Chart
        options={options}
        series={[{ data: tickerInfo.series }]}
        type="area"
        height={128}
        width={128}
      />
    </Paper>
  )
};

export default LiveTicker;