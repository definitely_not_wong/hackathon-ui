import React, { ReactNode, useEffect, useRef, useState } from 'react';
import { Button, ButtonBase, GridList, GridListTile, Paper, Typography } from '@material-ui/core';
import LiveTicker from './liveTicker';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import NewLiveTickerDialog from './newLiveTickerDialog';
import { TradeType } from '../../types';
import useSize from '../../utils/useSize';

interface WatchlistProps {
  portfolio: TradeType[];
}


const useStyles = makeStyles({
  liveTickers: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  addToWatchList: {
    width: 168,
    height: 168,
    display: 'flex',
    flexDirection: 'column',
    border: '2px grey solid',
    margin: '8px 0 8px 21.33px',
    borderRadius: 4,
    boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12);'
  }
});
const ADD_SIZE = 48;
const TRACKER_SIZE = 168;

const Watchlist = ({ portfolio }: WatchlistProps) => {
  const classes = useStyles();

  const [liveTickers, setLiveTickers] = useState<ReactNode[]>([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [padding, setPadding] = useState(0);

  const bodyContainer = useRef<HTMLElement>();

  const size = useSize(bodyContainer);


  const addTicker = (tickerName: string) => {
    if (tickerName !== '') {
      setLiveTickers(prevState => (
        [...prevState,
          <LiveTicker tickerName={tickerName}/>
        ]
      ));
    }

    setOpenDialog(false);
  };

  useEffect(() => {
    if (size) {
      const { width } = size;
      const numberOfTrackers = Math.floor(width / TRACKER_SIZE);
      console.log(size, numberOfTrackers)
      if (numberOfTrackers === 0) {
        setPadding(0)
      } else {
        setPadding(( width - numberOfTrackers * TRACKER_SIZE) / (numberOfTrackers + 1));
      }
    }
  }, [size]);

  return (
    <Paper ref={bodyContainer}>
      <Typography variant="h5" component="h2" style={{ padding: 16}}>
        My Watchlist
      </Typography>
      <div className={classes.liveTickers}>
        {liveTickers.map((ticker) => (
          <div style={{ marginLeft: padding }}>
            {ticker}
          </div>
        ))}
        <ButtonBase
          style={{ marginLeft: padding }}
          className={classes.addToWatchList}
          focusRipple
          onClick={() => {setOpenDialog(true)}}
        >
          <Typography variant="h6">
            Add to watchlist
          </Typography>
          <AddIcon style={{ height: ADD_SIZE, width: ADD_SIZE }}/>
        </ButtonBase>
      </div>
      <NewLiveTickerDialog open={openDialog} confirm={addTicker} portfolio={portfolio}/>
    </Paper>
  );
};

export default Watchlist;