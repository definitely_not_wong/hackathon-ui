import { Ref, useEffect, useState, useRef, useCallback, MutableRefObject } from 'react';

export default function useSize(elRef: MutableRefObject<HTMLElement | undefined> ) {
  const [size, setSize] = useState<{width: number, height: number} | null>(null);

  const onWindowResize = useCallback(() => {
    if (elRef.current) {
      const {width, height} = elRef.current.getBoundingClientRect();
      setSize({
        width, height
      })
    }
  }, [elRef]);
  
  useEffect(() => {
    window.addEventListener('resize', onWindowResize);
    
    return () => {
      window.removeEventListener('resize', onWindowResize);
    }
  }, [onWindowResize]);

  useEffect(() => {
    onWindowResize()
  }, [elRef]);
  
  return size;
}