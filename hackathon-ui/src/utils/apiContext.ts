import {Component, createContext} from "react";
import axios from 'axios';
import io from 'socket.io-client';

import {BollingerDataResponse, StockInfoType, TickerNameResult, TradeAction, TradeType} from "../types";
import convertBollingerData from './convertBollingerData';
import { TermType } from './termType';

type LiveTradingArguments= {
    tickerName: string;
    datePeriod?: string;
    interval?: string;
}

type TickerNameResponse = {
    ResultSet: {
        Query: string;
        Result: TickerNameResult[];
    }
}

const PORTFOLIO_API = 'http://docker13.conygre.com:8090';
const TRADE_API = 'http://localhost:8080';
const LIVE_DATA_API = 'http://127.0.0.1:5000';
export const LIVE_PRICE_EVENT = 'live ticker';

export const ApiContext = createContext({} as Api);

class Api {

    socket: SocketIOClient.Socket;

    constructor() {
        this.socket = io.connect(LIVE_DATA_API)
    }

    getStockInfo(tickerName: string): Promise<StockInfoType | null> {
        return axios
          .get(`${LIVE_DATA_API}/ticker?ticker=${tickerName}`, {
              headers: {
                  'Access-Control-Allow-Origin': '*',
              }})
          .then((response) => response.data)
    }

    createTrade(tickerName: string, volume: number, price: number, tradeAction: TradeAction) {
        console.log('tickername', tickerName);
        return axios
          .post(`${TRADE_API}/trades/${tradeAction.toUpperCase()}/${tickerName}/${volume}/${price}/1`, {
              headers: {
                  'Access-Control-Allow-Origin': '*',
              },
          })
          .then((response) => (
            response.data
          ))
          .then((data) => {
              console.log(data);
              return data;
          })
    }

    getPortfolio(): Promise<TradeType[]> {
        return axios
          .get(`http://localhost:8090/portfolio/1/all`)
          .then((response) => {
              console.log(response.data)
              return response.data;
          });
    }

    getLiveTradeHistory({tickerName, datePeriod = '', interval = ''}: LiveTradingArguments) {
        const path = `${LIVE_DATA_API}/history?ticker_name=${tickerName}&date_period=${datePeriod}&interval=${interval}`;
        return axios
          .get(path, {
              headers: {
                  'Access-Control-Allow-Origin': '*',
              },
          })
          .then((response) => (
            response.data
          ));
    }

    getTickerName(tickerName: string): Promise<TickerNameResult[]> {
        const path = `${LIVE_DATA_API}/sp500?ticker_name=${tickerName}`;
        return axios
          .get<TickerNameResponse>(path, {
              headers: {
                  'Access-Control-Allow-Origin': '*',
              }
          })
          .then((response) => (
            response.data.ResultSet.Result
          ));
    }

    getLivePriceSocket () {
        return this.socket;
    }

    getCurrentPrice(ticker: string) {
        return 100;
    }

    getBollingerData(tickerName: string, term: TermType = 'short') {
        const path = `${LIVE_DATA_API}/bollinger?ticker_name=${tickerName}&term=${term}`;
        return axios
          .get<BollingerDataResponse>(path, {
              headers: {
                  'Access-Control-Allow-Origin': '*',
              }
          })
          .then((response) => (
            response.data
          ))
          .then((data: BollingerDataResponse) => {
              return convertBollingerData(data, term)
          })
    }
}

export default Api;
