import NumberFormat from "react-number-format";
import React from 'react';

interface CurrencyInputProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: {
    target: { name: string; value: string },
    persist: () => void;
  }) => void;
  name: string;
}

const CurrencyInput = (props: CurrencyInputProps) => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name as string,
            value: values.value,
          },
          persist: () => {}
        });
      }}
      thousandSeparator
      prefix="$"
      isNumericString
      allowNegative={false}
      decimalScale={2}
      fixedDecimalScale={true}
    />
  );
};

export default CurrencyInput;