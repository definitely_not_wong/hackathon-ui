import NumberFormat from "react-number-format";
import React from 'react';

interface VolumeInputProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: {
    target: { name: string; value: string },
    persist: () => void;
  }) => void;
  name: string;
}

const VolumeInput = (props: VolumeInputProps) => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name as string,
            value: values.value,
          },
          persist: () => {}
        });
      }}
      thousandSeparator
      isNumericString
      allowNegative={false}
      decimalScale={5}
      fixedDecimalScale={true}
    />
  );
};

export default VolumeInput;