import {TradeHistoryType, TradeType} from "../types";
import {Order} from "./sortableHeader";
import {TradeTypeKeys} from "../components/TickerInfo/tradeHistory";

function descendingComparator(a: any, b: any, orderBy: keyof TradeHistoryType) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function descendingDateComparator(a: TradeHistoryType, b: TradeHistoryType) {
    const { date: aGoalDate = '0' } = a;
    const { date: bGoalDate = '0' } = b;

    const aDate = new Date(aGoalDate).getTime();
    const bDate = new Date(bGoalDate).getTime();

    if (bDate < aDate) {
        return -1;
    }
    if (bDate > aDate) {
        return 1;
    }
    return 0;
}

export function getComparator(
    order: Order,
    orderBy: TradeTypeKeys,
): (a: TradeHistoryType, b: TradeHistoryType) => number {
    const orderModifier = order === 'desc' ? 1 : -1;

    if (orderBy === 'date') {
        return (a, b) => orderModifier * descendingDateComparator(a, b);
    }

    return (a, b) => orderModifier * descendingComparator(a, b, orderBy);
}

function sortRows<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    console.log(stabilizedThis);
    return stabilizedThis.map((el) => el[0]);
}

export default sortRows;
