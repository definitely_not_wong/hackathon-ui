export const TERM_TYPES = ['short', 'medium', 'long'] as const;

type TermTuple = typeof TERM_TYPES;
export type TermType = TermTuple[number];