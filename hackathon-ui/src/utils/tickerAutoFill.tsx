import React, { useContext, useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete, { AutocompleteChangeReason, AutocompleteInputChangeReason } from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';

import { ApiContext } from './apiContext';
import { TickerNameResult } from '../types';
import useDebounceValue from './useDebounce';

interface TickerAutoFillProps {
  selectTickerName: (tickerName: string) => void;
  initialOptions?: TickerNameResult[];
}


export default function TickerAutoFill({ selectTickerName, initialOptions = [] }: TickerAutoFillProps) {
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState<TickerNameResult[]>(initialOptions);
  const [tickerName, setTickername] = useState('');
  const [loading, setLoading] = useState(false);

  const apiContext = useContext(ApiContext);

  const debouncedNameFilter = useDebounceValue(tickerName, 500);

  const onTickerSelect = (
    event: React.ChangeEvent<{}>,
    value: TickerNameResult | string | null,
    reason: AutocompleteChangeReason | AutocompleteInputChangeReason
  ) => {
    if (typeof value !== 'string' && value !== null){
      setTickername(value.symbol);
    } else if (typeof value === 'string' ) {
      setTickername(value);
    }
    setLoading(true);
  };

  const onSubmit = (
    event: React.ChangeEvent<{}>,
    value: TickerNameResult | string | null,
    reason: AutocompleteChangeReason | AutocompleteInputChangeReason
  ) => {
    console.log(value);
    if (typeof value !== 'string' && value !== null){
      selectTickerName(value.symbol);
    } else if (typeof value === 'string' ) {
      selectTickerName(value);
    }
  };

  useEffect(() => {
    let active = true;

    if (active) {
      console.log('searching for', debouncedNameFilter);
      apiContext
        .getTickerName(debouncedNameFilter)
        .then((tickerNames) => {
          if (tickerNames.length !== 0) {
            setOptions(tickerNames);
          }
          setLoading(false);
        });
    }

    return () => {
      active = false;
    };
  }, [apiContext, debouncedNameFilter]);

  useEffect(() => {
    if (!open) {
      setOptions(initialOptions);
    }
  }, [open]);

  return (
    <Autocomplete
      freeSolo
      id="asynchronous-demo"
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      openOnFocus
      getOptionSelected={(option, value) => option.symbol === value.symbol}
      getOptionLabel={(option) => option.symbol || ''}
      options={options}
      loading={loading}
      onChange={onSubmit}
      onInputChange={onTickerSelect}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Ticker"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
