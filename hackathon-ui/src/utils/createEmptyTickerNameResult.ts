import { TickerNameResult } from '../types';

export const createEmptyTickerNameResult = (tickerName: string): TickerNameResult => ({
  symbol: tickerName,
  name: tickerName,
  exch: '',
  type: '',
  exchDisp: '',
  typeDisp: ''
});