import React, { CSSProperties } from 'react';
import { TableCell, TableHead, TableSortLabel } from '@material-ui/core';

import {TradeTypeKeys} from "../components/TickerInfo/tradeHistory";

export type Order = 'asc' | 'desc';

export interface HeadCell {
    disablePadding: boolean;
    id: TradeTypeKeys;
    label: string;
    align: 'inherit' | 'left' | 'center' | 'right' | 'justify';
}

interface SortableHeaderProps {
    headCells: HeadCell[];
    numSelected?: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: TradeTypeKeys) => void;
    onSelectAllClick?: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount?: number;
}

const visuallyHidden: CSSProperties = {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
};

const SortableHeader = ({ headCells, onRequestSort, order, orderBy }: SortableHeaderProps) => {
    const createSortHandler = (property: TradeTypeKeys) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            {headCells.map((headCell) => (
                <TableCell
                    key={headCell.id}
                    align={headCell.align}
                    padding={headCell.disablePadding ? 'none' : 'default'}
                    sortDirection={orderBy === headCell.id ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : 'asc'}
                        onClick={createSortHandler(headCell.id)}
                    >
                        {headCell.label}
                        {orderBy === headCell.id ? (
                            <span style={visuallyHidden}>
                              {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                            </span>
                        ) : null}
                    </TableSortLabel>
                </TableCell>
            ))}
        </TableHead>
    )
};

export default SortableHeader;
