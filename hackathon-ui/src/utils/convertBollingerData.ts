import { BollingerData, BollingerDataResponse } from '../types';
import { TermType } from './termType';


const TERM_TYPE: {[id: string]: number} = {
  'short': 10,
  'medium': 20,
  'long': 50
};
const AVG = 0;
const UPPER = 1;
const LOWER = 2;
const CLOSE = 3;

const convertBollingerData = (newBollingerData: BollingerDataResponse, term: TermType): BollingerData => {
  const { index, data } = newBollingerData;
  const bollingerData: BollingerData = {
    type: `${TERM_TYPE[term]}d mavg`,
    avg: [],
    upperBand: [],
    lowerBand: [],
    close: []
  };

  index.forEach((dateValue, index) => {
    bollingerData.avg.push([dateValue, data[index][AVG]]);
    bollingerData.upperBand.push([dateValue, data[index][UPPER]]);
    bollingerData.lowerBand.push([dateValue, data[index][LOWER]]);
    bollingerData.close.push([dateValue, data[index][CLOSE]]);
  });

  console.log(bollingerData);

  return bollingerData;
};

export default convertBollingerData;