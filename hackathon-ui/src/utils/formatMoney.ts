const usdFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
});

const formatMoney = (value: number = 0) => (
    usdFormatter.format(value)
);

export default formatMoney;
