import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import * as serviceWorker from './serviceWorker';
import Api, { ApiContext } from "./utils/apiContext";

ReactDOM.render(
  <React.StrictMode>
      <ApiContext.Provider value={new Api()}>
          <App />
      </ApiContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
