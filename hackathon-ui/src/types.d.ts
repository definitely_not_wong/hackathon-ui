
export interface TradeHistoryType {
    id: {
        timestamp: number;
        date: string;
    }
    date: string;
    ticker: string;
    stockQuantity: number;
    requestedPrice: number;
    status: string;
    action: TradeAction
}

export interface TradeType {
    id: {
        timeStamp: number;
        date: string;
    };
    ticker: string;
    quantity: number;
    purchasePrice: number;
    owner: string;
    status: string;
    tradeHistory: TradeHistoryType[];
    pending: TradeHistoryType[];
}



export interface BollingerDataResponse {
    columns: string[];
    index: number[];
    data: number[][];
}

export interface BollingerData {
    type: string;           // 10d mavg, 20d mavg, 50d mavg
    avg: number[][];         // [time, value]
    upperBand: number[][];
    lowerBand: number[][];
    close: number[][]
}

export type TradeAction = 'Buy' | 'Sell';

export interface NewTradeInfo {
  tickerName: string;
  tradeAction: TradeAction;
  volume: number;
  price: number;
}

export type TickerNameResult = {
  symbol: string;
  name: string;
  exch: string;
  type: string;
  exchDisp: string;
  typeDisp: string;
}

// Pulled from live data
export type StockInfoType = {
  zip: string;
  sectors: string;
  previousClose: number;
  longBusinessSummary: string;
  open: number;
  bid: number;
  symbol: string;
  logo_url: string;
  website: string;
  shortName: string;
  longName: string;
}